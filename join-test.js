var m = require("./model.js");
var util = require("util");
var conn = m.connect;
var async = require("async");
var log = function(inLine){
	console.log(inLine);
}
var g = function (k,l, call){
	var p = {};
	p.phone_number = l.phone_number;
	p.district = k.district;
	p.description = k.description;
	p.status = k.status;
	p.price = k.price;
	p.date = k.date;
	p.coworker = k.coworker;
	p.client = k.client;
	p.address = k.address;
	call(p);
}
async.parallel(
	[
		function(callback){
			m.find({},m.Tasks, function(err, res){
				if(err){
					log("Error: " + err);
					return callback(err, null);
				}else{
					return callback(null, res);
				}
			});
		},
		function(callback){
			m.find({}, m.Employees, function(err, res){
				if(err){
					log("Error: " + err);
					return callback(err, null);
				}else{
					return callback(null, res);
				}
			});
		},

	],
	function done(error, results){
		var endArray = [];
		async.mapSeries(
			results[0],
			function(item, callback){
				async.mapSeries(
					results[1],
					function(itemU, callbackU){

						if(item.client == itemU._id){
							return callbackU(["end", item, itemU]);
						}else{
							return callbackU(null, null);
						}
					},
					function(errU, resU){
						if(errU && errU[0] == "end"){
							var l = errU[1];
							l.phone_number = errU[2].phone_number;
							g(errU[1], errU[2], function(re){
								callback(null, re);
							});							
						}else if(errU){
							callback(errU, null);
						}
					}
				);
			},
			function (err, res){
				
			}
		);
	}
);

