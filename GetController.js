var express = require("express");
var request = require("request");
var mongoose = require("mongoose");
var path = require("path");
var mod = require("./model.js");
var C = require("./config.js");
var async = require("async");
var util = require("util");
var connect = mod.connect;
var users = mod.Employees;
var tasks = mod.Tasks;
var server = express();
var KEY_API = C.KEY_API;
var FILE_LOG = C.FILE_LOG;
var ERROR_404 = C.ERROR_404;
var task = C.task.way;
var user = C.user.way;
var swig = require("./swig_temp.js");
var log = function(inLine){
	mod.writeLog(inLine, FILE_LOG);
};
function isEmpty(obj){
	if(obj == null || obj == ""){
		return true;
	}else{
		return false;
	}
}
function atInvalid(){
	log("Access token invalid.");
}
var checkAccessToken = function (access_token)
{
	if(access_token == KEY_API){
		return true;
	}else{
		return false;
	}
}
var getServer = function(io){
	server.get(
	"/" + task.one + "/",
	function(req, result)
	{
		if(req.query.access_token == KEY_API && !isEmpty(req.query._id)){
			log("Task ID: " + req.query._id);
			mod.getOne(
				req.query._id,
				tasks,
				function(error, res)
				{
					if(error){
						console.log(error);
						result.send(ERROR_404);
					}else{
						if(!isEmpty(res)){
							result.send(res);
						}else
							result.send(ERROR_404);
					}
				}
			);
		}else{
			result.send(ERROR_404);
		}		
	}
);
//*************************************************
server.get(
	"/" + task.list + "/",
	function(req, res)
	{
		var query = req.query;
		if(checkAccessToken(query.access_token)){
			mod.getAll(
				tasks,
				function(err, result){
					if(err){
						log(err + "\n Test.");
						res.send(ERROR_404);
					}else{
						res.send(result);	
					}
				}
			);
		}else{
			res.send(ERROR_404);
		}
	}
);
//*************************************************
server.get(
	"/" + task.accept + "/",
	function(req, res)
	{
		var query = req.query;
		if(query.access_token == KEY_API && !isEmpty(query.coworker_id) && !isEmpty(query.task_id)){
			var q_id = query.coworker_id;
			var u_id = query.task_id;
			if(q_id != "" && u_id != ""){
				mod.assignTask(
					q_id,
					u_id,
					function(error, result){
						if(error){
							log(error);
							res.send(C.ALERT_QUERY_ERROR);
						}else{
							log(result);
							res.send(C.OK);
						}
					}
				);
			}else{
				res.send(ERROR_404);
			}
		}else{
			res.send(ERROR_404);
		}
	}
);
//*************************************************
server.get(
	"/" + task.add + "/",
	function(req, res)
	{
		var query = req.query;
		log("Query: " + util.inspect(query));
		log("Address:*********************************** " + query.address);

		if(query.access_token == KEY_API 
			&& !isEmpty(query.district) 
			&& !isEmpty(query.description) 
			&& !isEmpty(query.price) 
			&& !isEmpty(query.date) 
			&& !isEmpty(query.client)){
			mod.insert(
				{
					district: query.district,
					description: query.description,
					price: query.price,
					date: query.date,
					client: query.client,
					status: false,
					address: isEmpty(query.address) ? "" : query.address,
					coworker: 0
				},
				tasks,
				function(error, result)
				{
					if(error){
						log(error);
						res.send(ERROR_404);
					}else{
						var tmp = "Add complete: " + result;
						res.send(tmp);						
						log(tmp);
						io.sockets.emit(task.new, '{"access_token":"' + KEY_API + '"}');
					}
				}
			);
		}else{
			res.send(ERROR_404);
		}
	}
 );
//*************************************************
server.get(
	"/" + task.remove + "/",
	function(req, res)
	{
		if(!checkAccessToken(req.query.access_token) ){
			res.send(ERROR_404);
		}else{
			var query = req.query;
			mod.remove(
				{ _id: query.id },
				tasks,
				function(err, result){
					if(err){
						log(err);
						res.send(ERROR_404);
					}else{
						io.sockets.emit(task.update, '{"access_token":"' + KEY_API + '"}');
						res.send(C.OK);
					}
				}
			);
		}
	}
);
server.get(
	"/" + task.update + "/",
	function(req, res){
		var q = req.query;
		if(
			!checkAccessToken(KEY_API)
			|| isEmpty(q.id)
			|| isEmpty(q.district)
			|| isEmpty(q.description)
			|| isEmpty(q.price)
			|| isEmpty(q.date)
			|| isEmpty(q.client)
			|| isEmpty(q.address)
		){
			res.send(ERROR_404);
		}else{
			mod.update(
				{_id: q.id}, 
				{
					district: q.district,
					description: q.description,
					address: q.address,
					price: q.price,
					date: q.date,
					client: q.client,
				}, 
				tasks, 
				false, 
				function(err, result){
					if(err){
						log(err);
						res.send(ERROR_404);
					}else{
						io.sockets.emit(task.update, '{"access_token":"' + KEY_API + '"}');
						res.send(C.OK);
					}
				}
			);
		}
	}
);
server.get(
	"/" + task.assign + "/",
	function(req, res){
		var q = req.query;
		if(
			!checkAccessToken(req.query.access_token)
		){
			res.send(ERROR_404);
		}else if(!isEmpty(q.user_id) && !isEmpty(q.task_id)){
			console.log("Test");
			mod.assignTask(
				q.user_id, 
				q.task_id, 
				function(error, result){
					if(error){
						res.send(ERROR_404);
					}else{
						io.sockets.emit(task.update, '{"access_token":"' + KEY_API + '"}');
						res.send(C.OK);
					}
				}
			);			
		}
	}
);
server.get(
	"/" + task.assigns + "/",
	function(req, res){
		var q = req.query;
		if(
			!checkAccessToken(q.access_token)
			&& isEmpty(q.user_id)
		){
			res.send(ERROR_404);
		}else{
			console.log(q.user_id);
			mod.isExists(
				{
					_id: q.user_id
				},
				users,
				function(error, result){
					if(error){
						console.log(error);
						res.send(ERROR_404);
					}else if(result){
						var g = function (k,l, call){
							var p = {};
							p.phone_number = l.phone_number;
							p.district = k.district;
							p.description = k.description;
							p.status = k.status;
							p.price = k.price;
							p.date = k.date;
							p.coworker = k.coworker;
							p.client = k.client;
							p.address = k.address;
							p._id = k._id;
							call(p);
						}
						async.parallel(
							[
								function(callback){
									mod.find({coworker: q.user_id},mod.Tasks, function(err, res){
										if(err){
											log("Error: " + err);
											return callback(err, null);
										}else{
											return callback(null, res);
										}
									});
								},
								function(callback){
									mod.find({group: "3"}, mod.Employees, function(err, res){
										if(err){
											log("Error: " + err);
											return callback(err, null);
										}else{
											return callback(null, res);
										}
									});
								},

							],
							function done(error, results){
								var endArray = [];
								async.mapSeries(
									results[0],
									function(item, callback){
										async.mapSeries(
											results[1],
											function(itemU, callbackU){

												if(item.client == itemU._id){
													return callbackU(["end", item, itemU]);
												}else{
													return callbackU(null, null);
												}
											},
											function(errU, resU){
												if(errU && errU[0] == "end"){
													var l = errU[1];
													l.phone_number = errU[2].phone_number;
													g(errU[1], errU[2], function(re){
														log(util.inspect(re));
														callback(null, re);
													});							
												}else if(errU){
													callback(errU, null);
												}
											}
										);
									},
									function (err, ress){
										res.send(ress);
									}
								);
							}
						);								
					}else{
						res.send(ERROR_404);
					}
				}
			);
		}
	}
);
server.get(
	"/" + task.status + "/",
	function(req, res){
		var q = req.query;
		if(
			!checkAccessToken(q.access_token)
			|| isEmpty(q.id)
			|| isEmpty(q.status)
		){
			res.send(ERROR_404);
		}else{
			mod.setStatusTask(
				q.id, 
				q.status,
				tasks,
				function(err, result){
					if(err){
						log(err);
						res.send(ERROR_404);
					}else{
						io.sockets.emit(task.update, '{"access_token":"' + KEY_API + '"}');
						log(result);
						res.send(C.OK);
					}
				}
			);
		}
	}
);
//*************************************************
server.get(
	"/" + user.auth + "/",
	function(req,res)
	{
		if((req.query.pass != null) && (req.query.login != null)){
			phone_number_in = req.query.login;
			password_in = req.query.pass;
			mod.getAuth(
				phone_number_in, 
				password_in, 
				function(err, result)
				{
					if(err){
						log(err);
						res.send(ERROR_404);
					}else{
						log(result);
						res.send(result._id.toString());
					}
				}
			);
		}else{
			res.send(ERROR_404);
		}
	}
);
//*************************************************
server.get(
	"/" + user.add + "/",
	function(req, res)
	{
		if(checkAccessToken(req.query.access_token )){
			query = req.query;
			if(
				!isEmpty(query.phone_number.replace(/[\ \+\(\)\-]/g,""))
				&& !isEmpty(query.password.replace(/[\ \+\(\)\-]/g,""))
				&& !isEmpty(query.first_name.replace(/[\ \+\(\)\-]/g,""))
			){
				mod.isExists(
					{phone_number: query.phone_number.replace(/\ /g,"")},
					users,
					function(err, res_e)
					{
						if(err){
							log(err);
							res.send(ERROR_404);
						}else{
							if(!res_e){
								mod.insert(
									{
										first_name: query.first_name,
										nickname: !isEmpty(query.nickname.replace(/[\ \+\(\)\-]/g,""))? query.nickname.replace(/[\ \+\(\)\-]/g,"") : "",
										phone_number: query.phone_number.replace(/[\ \+\(\)\-]/g,""),
										password: query.password.replace(/[\ \+\(\)\-]/g,""),
										group: !isEmpty(query.group.replace(/[\ \+\(\)\-]/g,""))? query.group : "0",
										notes: !isEmpty(query.notes.replace(/[\ \+\(\)\-]/g,""))? query.notes : "Заметка"
									},
									users,
									function(err, result)
									{
										if(err){
											log(err);
											res.status(404);
											res.send(ERROR_404);
										}else{
											log(result);
											res.send(C.OK);
										}
									}
								);
							}else{
								log(C.ALERT_QUERY_ROW_EXISTS);
								res.send(C.ALERT_QUERY_ROW_EXISTS);
							}
						}
					}
				);				
			}
		}else{
			res.status(404);
			res.send(ERROR_404);
		}
	}
);
//*************************************************
server.get(
	"/" + user.update + "/",
	function(req, res)
	{
		q = req.query;
		if(!checkAccessToken(req.query.access_token) || isEmpty(q.id)){
			res.status(404);
			res.send(ERROR_404);
		}else{
			if(
				isEmpty(q.password.replace(/\ /g,""))
				|| isEmpty(q.first_name.replace(/[\ \+\(\)\-]/g,""))
			){
			}else{
				mod.isExists(
					{phone_number: q.phone_number.replace(/\ /g,"") == "" ? "1234" : q.phone_number.replace(/[\ \+\(\)\-]/g,"")},
					users,
					function(err, res_e)
					{
						if(err){
							log(err);
							res.send(ERROR_404);
						}else{
							if(!res_e){
								var data = null;
								if(q.phone_number.replace(/\ /g,"") == ""){
									data = {
										first_name: q.first_name,
										nickname: !isEmpty(q.nickname.replace(/[\ \+\(\)\-]/g,""))? q.nickname.replace(/[\ \+\(\)\-]/g,"") : "",
										password: q.password,
										group: !isEmpty(q.group)? q.group : "0",
										notes: !isEmpty(q.notes)? q.notes : "Заметка"
									};
								}else{
									data = {
										first_name: q.first_name,
										nickname: !isEmpty(q.nickname.replace(/[\ \+\(\)\-]/g,""))? q.nickname.replace(/[\ \+\(\)\-]/g,"") : "",
										phone_number: q.phone_number.replace(/\ /g,"") == "" ? "number" : q.phone_number.replace(/[\ \+\(\)\-]/g,""),
										password: q.password,
										group: !isEmpty(q.group)? q.group : "0",
										notes: !isEmpty(q.notes)? q.notes : "Заметка"
									};
								}
								mod.update(
									{_id: q.id},
									data,
									users,
									false,
									function(err, result)
									{
										if(err){
											log(util.inspect(err));
											res.status(404);
											res.send(ERROR_404);
										}else{
											log("Error: " + util.inspect(result));
											res.send(C.OK);
										}
									}
								);
							}else{
								log("Error: " + util.inspect(C.ALERT_QUERY_ROW_EXISTS));
								res.send(C.ALERT_QUERY_ROW_EXISTS);
							}
						}
					}
				);	
			}
		}
	}
);
//*************************************************
server.get(
	"/" + user.remove + "/",
	function(req, res)
	{
		if(!checkAccessToken(req.query.access_token) ){//&& isEmpty(req.query.id
			res.send(ERROR_404);
		}else{
			var query = req.query;
			mod.remove(
				{ _id: query.id },
				users,
				function(err, result){
					if(err){
						log(err);
						res.send(ERROR_404);
					}else{
						log(result);
						io.sockets.emit(task.update, '{"access_token":"' + KEY_API + '"}');
						res.send(C.OK);
					}
				}
			);
		}
	}
);
//*************************************************
server.get(
	"/" + user.clients + "/",
	function(req, res){
		q = req.query;
		if(!checkAccessToken()){
			mod.find(
				{group: "3"},
				users,
				function(err, result){
					if(err){
						log(err);
						res.send(ERROR_404);
					}else{
						log(result);
						res.send(result);
					}
				}
			);
		}
	}
);
//*************************************************
mainUrl = "http://vm18366.hv8.ru:5000/static/a_t/?access_token=";
allUrls = {
	add: "http://vm18366.hv8.ru:5000/task/add/?access_token=" + KEY_API,
	current: "http://vm18366.hv8.ru:5000/static/a_t/?access_token=" + KEY_API
};
// *** Админ часть для задач. ***
server.get(
	'/' + C.ADMIN.way.index + '/', 
	function(req, res){
	var query = req.query;
	if(checkAccessToken(query.access_token)){
		server.use('/' + C.ADMIN.way.index + '/',express.static(path.join(__dirname + '/static/pcsupport')));
		mod.getAll(tasks, function(err, resmod){
			if(err){
				log(err);
				res.send(ERROR_404);
			}else{
				swig(
					{
						table: resmod, 
						api_key: KEY_API, 
						urls: allUrls, 
						PUBLIC: {
							URL: C.PUBLIC.URL.MAIN, 
							ADMIN: C.PUBLIC.URL.MAIN_ADMIN, 
							task_events: task, 
							user_events: user, 
							admin: C.ADMIN.way
						},
						PAGE :{
							TITLE: 'PCSupport - Обслуживание электроники в Улан-Удэ',
						}
					}, 
					__dirname + '/' + C.ADMIN.way.public_path + '/index.html', 
					function(result){
						res.send(result);
					}
				);
			}
		});		
	}else{
		res.send(ERROR_404);
	}
});
server.get(
	"/" + C.ADMIN.way.user + "/",
	function(req, res){
		var q = req.query;
		if(!checkAccessToken(q.access_token)){
			res.send(ERROR_404);
		}else{
			server.use('/' + C.ADMIN.way.user + '/',express.static(path.join(__dirname + '/static/pcsupport')));
			mod.getAll(users, function(err, resmod){
				if(err){
					log(err);
					res.send(ERROR_404);
				}else{
					swig(
						{
							table: resmod, 
							api_key: KEY_API, 
							urls: allUrls, 
							PUBLIC: {
								URL: C.PUBLIC.URL.MAIN, 
								user_events: user, 
								task_events: task,
								admin: C.ADMIN.way,
								exists: C.ALERT_QUERY_ROW_EXISTS
							},
							PAGE :{
								TITLE: 'PCSupport - Обслуживание электроники в Улан-Удэ',
							}
						}, 
						__dirname + '/' + C.ADMIN.way.public_path + '/user.html', 
						function(result){
							res.send(result);
						}
					);
				}
			});
		}
	}
);
return server;
}
module.exports = {
	server: getServer
};