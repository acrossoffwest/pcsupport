var request = require("request");
var fs = require("fs");
var cheerio = require("cheerio");
var TOKEN = "acdd6f1ffbfdfbc0091bf0e8d78f94da4353806d56e32da82c0f571b";
var urlList = "https://pddimp.yandex.ru/nsapi/get_domain_records.xml?token=" + TOKEN + "&domain=ibb.su";
var delDNS = "https://pddimp.yandex.ru/nsapi/delete_record.xml?token=" + TOKEN +"&domain=ibb.su&record_id=";
var mod = require("./model.js");
var FILENAME_ERROR = "./dnskiller_error.log";
var FILENAME_LOG = "./dnskiller_log" + randomInt(1000, 9999) +".log";
var oldIP = "176.9.6.212";
var exceptions = [
	"sibkap.ibb.su",
	"academy.ibb.su",
	"indra.ibb.su",
	"amrita.ibb.su",
	"sip.ibb.su",
	"hodzinskaya.ibb.su",
	"fondpmj2.ibb.su",
	"arprb.ibb.su",
	"bw1.ibb.su",
	"emsenter2.ibb.su",
	"devamo.ibb.su",
	"devbase.ibb.su",
	"devgefest3.ibb.su",
	"devbeautyform.ibb.su"
];
var delArr = [];
var error = function(line)
{
	mod.writeLog(line, FILENAME_ERROR);
};
var log = function(line)
{
	mod.writeLog(line, FILENAME_LOG);
};
request(
	urlList,
	function(err, response, body)
	{
		if(err){
			error(err);
		}else{

			getDelArray(body, null);
		}
	} 
);
function getDelArray(data, callback)
{
	$ = cheerio.load(data);
	$("record").each(
		function(i, elem){
			if($(this).html() == oldIP && exceptions.indexOf($(this).attr("domain")) == -1){
				request(
					delDNS + $(this).attr("id"),
					function(err, response, body)
					{
						if(err){
							error(err);
						}else{
							log(body);
						}
					}
				);
			}
		}
	);
}
function randomInt (low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}
