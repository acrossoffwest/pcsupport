var server = require("http").createServer();
var io = require("socket.io")(server);
var util = require("util");
io.on(
	"connection",
	function(socket)
	{
		console.log("Connection");
		socket.on(
			"event",
			function(data)
			{
				console.log("Data");
				console.log(data);
				socket.emit("event", "answer");
			}
		);
		socket.on(
			"disconnect",
			function()
			{
				console.log("Disconnect.");	
			}
		);
	}
);
server.listen(8000);