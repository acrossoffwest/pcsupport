var express = require("express");
var request = require("request");
var async = require("async");
var mongoose = require("mongoose");
var util = require("util");
var ai = require("mongoose-auto-increment");
var Schema = mongoose.Schema;
//Set schema to colletion 
var employeesSchema = new Schema({
	_id: {type: Number, ref: "_id"},
	firstName: String,
	secondName: String,
	skills: String
});
// Create connection to mongodb database
var connection = mongoose.createConnection("mongodb://localhost/pcsupport");
// Init auto increment
ai.initialize(connection);
employeesSchema.plugin(ai.plugin, {model: "employees", field: "_id"});
// Create connection to collection
var Employees = connection.model("employees", employeesSchema);

/*				Create record				*/
// Add record to collection
/*var nCoworker = new Employees({
	firstName: "12",
	secondName: "34",
	skills: "56"
});
nCoworker.save( function(err, result)
{
	if(err){
		console.log("Error: " + err);
	}else{
		console.log("Query result: " + result);
	}
});*/
/*				Read/find record				
Employees.find(
	{firstName: "12"}, 
	function(err, result)
	{
		if(err){
			console.log(err);
		}else{
			console.log(result);
		}
	} 
); */
Employees.find(
	{},
	function(err, result)
	{
		if(err || result.length == 1){
			console.log("Error: " + err);
		}else{
			console.log("Full collection: " + result.length);
		}
	} 
);
/*				Update record				*/
Employees.findOne(
	{firstName: "12"}, 
	function(err, result)
	{
		if(err){
			console.log("Error: " + err);
		}else{
			if(result != null){
				result.fisrtName = "New Name test";
				result.save(function(err){
					if(err)
						console.log("Error: " + err);
				});
				console.log("FindOne result: " + result);
			}else{
				console.log("Record not found")
			}
		}
	}
);
/*				Delete record				*/
Employees.remove(
	{firstName: "test name"},
	function(err)
	{
		if(err){
			console.log(err);
		}else{
			console.log("Deleted records.");
		}

	}
);
/*
nCoworker = new Employees(
	{
		firstName: "test name",
		secondName: "test second name",
		skills: "skills test"
	}
);
nCoworker.save(
	function(err, res)
	{
		if(err){
			console.log("Error: " + err);
		}else{
			console.log("Row Added: " + res);
		}
	}
);*/