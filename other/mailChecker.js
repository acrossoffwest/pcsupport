var mod = require("./model.js");
var C = require("./config.js");
var mails = require("./mails.js");
var util = require("util");
var fs = require("fs");
var liteDB = "data/db/base.db";
var sql = require("sqlite3").verbose();
var async = require("async");
function out(inLine){
	console.log(inLine);
}



arrayToCSV("./full_mails_unique.js",
	function(result){
		mod.writeLog(result, "all_unique_mails.csv");
	}
);

function arrayToCSV(fileArray, callback){
	var array = require(fileArray);
	var length = array.length - 1;
	var endResult = "";
	array.forEach(
		function (item, i, array){
			if(length != i){
				endResult += item[0] + ";" + item[1] + "\n";
			}else{
				return callback(endResult);
			}
		}
	);
};


function setNames(nullArray, tmpNames, tmpCurrentIndex, callback){
	var test_0003 = require(nullArray);
	var tmp_mails = require(tmpNames);
	var lt = test_0003.length - 1;
	test_0003.forEach(
		function(item, i, test_0003){
			if(lt != i){
				if(item[0] == ""){
					test_0003[i][0] = tmp_mails[++tmpCurrentIndex];
				}
			}else{
				return callback(test_0003);
			}
		}
	);
}

/*setNames("./test_0003.js", "./tmp_mais.js", 0, function(res){
	mod.writeLog("module.exports = " + util.inspect(res) + ";", "full_mails_unique.js");
});
*/



function getNames(limit, callback){
	var dbFile = "./data/db/base.db";
	var dbFileExists = fs.existsSync(dbFile);
	var tmpArray = [];
	if(dbFileExists){
		var db = new sql.Database(dbFile);
		db.serialize(
			function(){
				if(dbFileExists){
					db.each(
						"SELECT * FROM `firm` LIMIT " + limit,
						function(err, row){
							if(!err){
								tmpArray.push(row.name);
							}
						},
						function(){
							callback(tmpArray);
						}
					);
				}
			}
		);
	}
}
/*getNames(10000, function(res){
	mod.writeLog("module.exports = " + util.inspect(res) + ";", "tmp_mais.js");
});*/
/*
var w = require("./mails.js");
var l = w.length - 1;
var newW = [];
var res = "";
w.forEach(
	function(item, i, w){
		if(item != null){
			if(item[1].indexOf(',') == -1){
				if(res.indexOf(" :m:" + item[1].toLowerCase() + ":m: ") == -1){
					res += "Iterator: " + i + " " + " :m:" + item[1].replace(/\ /g, "").toLowerCase() + ":m: " + "\n";
					push(item, function(){  });		
				}
			}
			else{
				itemPush(
					item,
					item[1].split(","),
					function(){
						out("end iterate");
					}
				);
			}
		}
		if(i == l){
			if(res.indexOf(" :m:" + item[1].toLowerCase() + ":m: ") == -1){
				res += "Iterator: " + i + " " + " :m:" + item[1].replace(/\ /g, "").toLowerCase() + ":m: " + "\n";
				push(item, function(){				  
				 return endArray(newW);	
				});
			}else{
				return endArray(newW);
			}
		}
			
	}
);
*/

function itemPush(mainItem, itemArray, callback){
	itemArray.forEach(
		function (item, i, itemArray){
			if(itemArray.length - 1 == i){
				if(res.indexOf(" :m:" + item.replace(/\ /g, "").toLowerCase() + ":m: ") == -1){
					res += "Iterator: " + i + " " + " :m:" + item.replace(/\ /g, "").toLowerCase() + ":m: " + "\n";
					push([mainItem[0], item.replace(/\ /g, "").toLowerCase()], function(){  });	
				}
				return callback("end");
			}else{
				if(res.indexOf(" :m:" + item.replace(/\ /g, "").toLowerCase() + ":m: ") == -1){
					res += "Iterator: " + i + " " + " :m:" + item.replace(/\ /g, "").toLowerCase() + ":m: " + "\n";
					push([mainItem[0], item.replace(/\ /g, "").toLowerCase()], function(){  });	
				}
			}
		}
	);
};
function push(item, callback){
	newW.push(item);
	callback();
};
function stringPlus(result, callback){
	res += result;
	callback();
}
function endArray(result){
	mod.writeLog(util.inspect(result), "test_0003.js");
	mod.writeLog(res, "test_0003_res.js");
	out("Length W: " + l);
	
}
function kill(){
	process.kill(process.pid);
};


