var mongoose = require("mongoose");
var async = require("async");
var Schema = mongoose.Schema;
const QUERY_DONE = "Query done.";
var fs = require("fs");
var C = require("./config.js");
var dbName = "pcsupport";
var User = new Schema(
	{
		first_name: String,
		second_name: String,
		nickname: String,
		phone_number: String,
		password: String,
		group: String,//1- модераторы, 2 - исполнители, 3 - клиент
		notes: String
	}
);
var Task = new Schema(
	{
		district: String,
		description: String,
		status: String,
		price: String,
		date: String,
		coworker: String,
		client: String,
		address: String
	}
);

var connect = mongoose.createConnection("mongodb://localhost/" + dbName);
var modUser = connect.model("user", User);
var modTask = connect.model("task", Task);
var insert = function(data, model, callback)
{
	var newRec = new model(data);
	newRec.save(function(err,res)
		{
			if(err){
				callback(err, null);
			}else{
				writeLog("Insert: " + res, "insert.log");
				callback(null, QUERY_DONE);
			}
		}
	);
};
var update = function(conditions, data, model, multi, callback)
{
	model.update(
		conditions, 
		data, 
		{multiple: multi}, 
		function(err, res)
		{
			if(err){
				writeLog("Error: " + err, "update.log");
				callback(err, null);
			}else{
				writeLog("Update: " + res, "update.log");
				callback(null, res);
			}
		}
	);
};
var setStatusTask = function(id, state,model, callback){
	model.update(
		{_id: id},
		{status: state},
		{multiple: false},
		function(err, res){
			if(err){
				callback(err, null);
			}else{
				callback(null, res);
			}
		}
	);
};
var remove = function(conditions, model, callback)
{
	model.remove(
		conditions, 
		function(err, res){
			if(err){
				callback(err, null);
			}else{
				callback(null, QUERY_DONE);
			}
		}
	);
};
var getAll = function (model, callback)
{
	model.find(
		{},
		function(err, res)
		{
			if(err){
				callback("Error get all record", 0);
			}else{
				callback(null, res);
			}
		} 
	).sort({_id: "-1"});	
};
var find = function(conditions, model, callback)
{
	model.find(
		conditions,
		function(err, result)
		{
			if(err){
				callback(err, null);
			}else{
				callback(null, result);
			}
		}
	).sort({_id: "-1"});
}
var getOne = function(findId, model, callback)
{
	model.findOne(
		{_id: findId},
		function(err, result)
		{
			if(err){
				callback(err, null);
			}else{
				callback(null, result);
			}
		}
	);
};
var writeLog = function writeLog(inLine, fileName){
	fs.exists(
		fileName,
		function(exists)
		{
			if(exists){
				fs.appendFile(
					fileName,
					"\n" + inLine,
					function(error)
					{
						if(error){
							console.error(error);
						}else{
							console.log("Log write: " + inLine);
						}
					}
				);
			}else{
				fs.writeFile(
					fileName,
					"\n" + inLine,
					function(error)
					{
						if(error){
							console.error(error);
						}else{
							console.log("Log write: " + inLine);
						}
					}
				);
			}
		}
	);
}
var getAuth = function (phone_number_in, password_in, callback) 
{
	find(
		{
			phone_number: phone_number_in,
			password: password_in	
		},
		modUser,
		function(err, result)
		{
			if(err){
				callback(err, null);
			}else if(result.length != 0){
				callback(null, result[0]);
			}else{
				callback(C.ERROR_404, null);
			}
		}
	);
}
var isUnique = function (conditions, model, callback) 
{
	find(
		conditions,
		model,
		function(err, result)
		{
			if(err){
				callback(err, null);
			}else if(result.length != 0 && result.length == 1){
				callback(null, true);
			}else{
				callback(null, false);
			}
		}
	);
};
var isExists = function (conditions, model, callback) 
{
	find(
		conditions,
		model,
		function(err, result)
		{
			if(err){
				callback(err, null);
			}else if(result.length != 0){
				callback(null, true);
			}else{
				callback(null, false);
			}
		}
	);
};
var assignTask = function( coworker_id, task_id, callback)
{
	isExists(
		{
			_id: coworker_id
		},
		modUser,
		function(err, res)
		{
			if(err){
				callback(err, null);
			}else{
				if(!res){
					callback(err, null);
				}else{
					isExists(
						{
							_id: task_id
						},
						modTask,
						function(err_1, res_1)
						{
							if(err_1){
								console.error("Error: " + err_1);
								callback(err_1, null);
							}else{
								if(!res_1){
									callback(C.ALERT_QUERY_ROW_NOT_EXISTS, null);
								}else{
									update(
										{
											_id: task_id
										}, 
										{
											coworker: coworker_id
										}, 
										modTask, 
										false,
										function(error, result)
										{
											if(error){
												callback(error, null);
											}else{
												writeLog("Task ID: " + task_id + "\nCoworker ID: " + coworker_id + "\n" + result, C.task.logs.ASSIGN);
												callback(null, result);
											}
										}
									);
								}
							}
						}
					);
				}
			}
		}
	);
};
var getUrl = function(mainUrl, method, key, params, callback){//params = [{key: '', value=''},...]
	if(!Array.isArray(params)){
		callback(err, null);
	}else{
		var url = mainUrl + method + "/?access_token=" + key;
		params.forEach(function(item, i){
			if(params.length - 1 == i){
				url += "&" + item.key + "=" + item.value;
				return callback(null, url);
			}else{
				url += "&" + item.key + "=" + item.value;
			}
		});
	}
};

/*
var Avito = new Schema(
	{
		phone: String,
		name: String,
		sName: String,
		ag: String,
		district: String
	}
);
var Avito1 = new Schema(
	{
		phone: String,
		name: String,
		sName: String,
		ag: String,
		district: String,
		date: String
	}
);
var Mails = new Schema(
	{
		mail: String,
		phone: String,
		company: String,
		name: String
	}
);

var modAv = connect.model("avito", Avito);
var modAv1 = connect.model("avito_new", Avito1);
var modMails = connect.model("mail", Mails);*/
module.exports = {
	connect: connect,
	Employees: modUser,
	Tasks: modTask,	
	insert: insert,
	update: update,
	remove: remove,
	getAll: getAll,
	getOne: getOne,
	find: find,
	writeLog: writeLog,
	/*avito: modAv,
	avito1: modAv1,
	mails: modMails,*/
	getAuth: getAuth,
	isUnique: isUnique,
	isExists: isExists,
	assignTask: assignTask,
	getUrl: getUrl,
	setStatusTask: setStatusTask
};



/*	
	db.createCollection(
		"user",
		{
			"validator":
				{
					$and:
						[
							{
								first_name: { "$type" : "string" },
								second_name: { "$type" : "string" },
								nickname: { "$type" : "string" },
								phone_number: { "$type" : "string" },
								password: { "$type" : "string" },
								group: { "$type": "string" },
								notes: { "$type": "string" }
							}
						]
				}
		}
	);
	db.createCollection(
		"task",
		{
			"validator":
			{
				$and: 
					[
						{
							district: { "$type" : "string" },
							discription: {"$type" : "string"},
							price: {"$type":"string"},
							date: {"$type" : "string"},
							coworker: {"$type" : "string"},
							client: {"$type" : "string"}
						}
					]
			}
		}
	);
	db.createCollection(
		"mails",
		{
			"validator":
			{
				$and: 
					[
						{
							mail: { "$type" : "string" },
							company: {"$type" : "string"},
							name: {"$type":"string"},
							phone: {"$type" : "string"}
						}
					]
			}
		}
	);
*/