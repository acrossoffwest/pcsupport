var express = require("express");
var request = require("request");
var mongoose = require("mongoose");
var path = require("path");
var mod = require("./model.js");
var C = require("./config.js");
var async = require("async");
var util = require("util");
var connect = mod.connect;
var users = mod.Employees;
var tasks = mod.Tasks;
var server = require("./GetController.js");
var KEY_API = C.KEY_API;
var FILE_LOG = C.FILE_LOG;
var ERROR_404 = C.ERROR_404;
var task = C.task.way;
var user = C.user.way;
var log = function(inLine){
	mod.writeLog(inLine, FILE_LOG);
};
function isEmpty(obj){
	if(obj == null || obj == ""){
		return true;
	}else{
		return false;
	}
}
function atInvalid(){
	log("Access token invalid.");
}
var checkAccessToken = function (access_token)
{
	if(access_token == KEY_API){
		return true;
	}else{
		return false;
	}
}
//*************************************************
var serverSocket = require("http").createServer();
var io = require("socket.io")(serverSocket);
var util = require("util");
io.on(
	"connection",
	function(socket)
	{
		log("Connect client.");
		socket.on(
			task.list,
			function(data)
			{	
				data = JSON.parse(data);
				if(data.access_token == KEY_API && data.free == 'true'){
					mod.find(
						{
							coworker: 0
						},
						tasks,
						function(error, result)
						{	
							if(error){
								mod.writeLog(
									error,
									FILE_LOG
								);
							}else{
								socket.emit(
									task.list,
									result
								);
							}
						}
					);
				}else{
					atInvalid();
				}
			}
		);
		socket.on(
			task.one,
			function(data)
			{
				data= JSON.parse(data);
				if(data.access_token == KEY_API && data.findId != null){
					mod.getOne(
						data.findId,
						tasks,
						function(error, result)
						{
							if(error){
								log(error);
							}else{
								socket.emit(task.one, result);
							}
						}
					);
				}else{
					atInvalid();
				}
			}
		);
		socket.on(
			task.new,
			function(data)
			{
				//io.sockets.emit(task.list, data);
			}
		);
		socket.on(
			"disconnect",
			function()
			{
				log("Client disconnect.");
			}
		);
	}
);
module.exports = {
	server: serverSocket,
	io: io
};