var CONFIG = {
	task : {
		title: "task", 
		way: {},
		logs: {
			ASSIGN: "./TaskAssign.LOG",
			ADD: "./TaskAdd.LOG",
			UPDATE: "./TaskUpdate.LOG",
			DELETE: "./TaskDelete.LOG",
		}
	},
	user : { 
		title: "user",
		way : {}
	},
	KEY_API : "asdfhlejo;hwnoq-0[bis-p[gj234",
	FILE_LOG : "./log.txt",
	LOGS: {
		TASK: "AssignTask.LOG",
		ADD_TASK: "AssignTask.LOG",
		ASSIGN_TASK: "AssignTask.LOG",
	},
	ERROR_404 : {error: "404"},
	ALERT_QUERY_ROW_EXISTS: {
		error: "Sorry, but row exists."
	},
	ALERT_QUERY_ERROR: {
		error: "Query error"
	},
	OK: {
		status: "OK"
	},
	ALERT_QUERY_ROW_NOT_EXISTS: {
		error: "Row not found."
	},
	PUBLIC: {
		URL_MAIN: "vm18366.hv8.ru",
		URL: {},
		PORT: 5000
	},
	ADMIN:{}
};
CONFIG.task.way = {
	list: CONFIG.task.title + "/list", 
	one: CONFIG.task.title + "/one",
	add: CONFIG.task.title + "/add",
	remove: CONFIG.task.title + "/remove",
	update: CONFIG.task.title + "/update",
	assigns: CONFIG.task.title + "/assigns",
	assign: CONFIG.task.title + "/assign",
	accept: CONFIG.task.title + "/accept",
	new: CONFIG.task.title + "/new",
	status: CONFIG.task.title + "/status"
};
CONFIG.user.way = {
	list: CONFIG.user.title + "/list", 
	one: CONFIG.user.title + "/one",
	add: CONFIG.user.title + "/add",
	remove: CONFIG.user.title + "/remove",
	update: CONFIG.user.title + "/update",
	auth: CONFIG.user.title + "/auth",
	clients: CONFIG.user.title + "/clients"
}
CONFIG.ADMIN = {
	way: {
		index: "static/a_t",
		user: "static/user",
		public_path: "static/pcsupport"
	}
};
CONFIG.PUBLIC.URL = {
	MAIN: "http://" + CONFIG.PUBLIC.URL_MAIN + ":" + CONFIG.PUBLIC.PORT + "/",
	MAIN_ADMIN: null,
	MAIN_ADMIN_USER: null
};
CONFIG.PUBLIC.URL.MAIN_ADMIN = "http://" + CONFIG.PUBLIC.URL_MAIN + ":" + CONFIG.PUBLIC.PORT + "/" + CONFIG.ADMIN.way.index + "/";

module.exports = CONFIG;