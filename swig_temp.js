var swig = require('swig');

module.exports = function(data, file, callback){
	var template = swig.compileFile(file);//'/static/pcsupport/index.html'
	callback(template(data));
};
